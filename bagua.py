

#
# 文本转为8进制数组
#
def strTo8bArr(str):
    # 返回数组
    arr = []
    # 将字符串转为byte数组
    nByte = bytes(str,encoding='utf-8')
    # 遍历byte数组 转为八进制
    for n2 in nByte:
        arr.append(oct(n2))

    return arr

#
# 八进制转八卦符
#
def toBaGua(strArr):
    code = {'0': '☰',  # 乾
            '1': '☱',  # 兑
            '2': '☲',  # 离
            '3': '☳',  # 震
            '4': '☴',  # 巽
            '5': '☵',  # 坎
            '6': '☶',  # 艮
            '7': '☷',  # 坤
            }

    # 返回八卦符
    returnStr = ''


    for b in strArr:
        # 去掉 0o
        bStr = b.replace('0o','')
        # 如果小于3位,前面补0
        if len(bStr) < 3:
            bStr = '0'+bStr

        for s in bStr:
            returnStr += code[s]

    return returnStr


#
# 8进制转文本
#
def bArrToStr(bArr):

    nByte = b''
    for b in bArr:
        nByte += chr(int(b,base=8)).encode('raw_unicode_escape')

    return bytes.decode(nByte)


#
# 八卦符转八进制
#
def to8bArr(baguaStr):
    code = {'☰':'0',  # 乾
            '☱':'1',  # 兑
            '☲':'2',  # 离
            '☳':'3',  # 震
            '☴':'4',  # 巽
            '☵':'5',  # 坎
            '☶':'6',  # 艮
            '☷':'7',  # 坤
            }

    bArr = []

    temp = []
    # 把八卦符转为8进制数字
    for s in baguaStr:
        temp.append(code[s])

    tempStr = ''
    # 数字3个一组 组合回八进制
    for i in range(len(temp)):

        tempStr += temp[i]

        if i % 3 == 2:
            bArr.append('0o'+tempStr)
            tempStr = ''


    return bArr
if __name__ == '__main__':

    arr = strTo8bArr('哇卡卡卡#$%^&*()')
    print(toBaGua(arr))
    #☰☵☰☳☴☴☲☷☴☲☳☲☳☴☶☲☰☴☲☱☷☳☴☳☲☰☰☲☰☲☰☶☱

    arr = to8bArr('☳☴☵☲☲☳☲☰☷☳☴☵☲☱☵☲☴☱☳☴☵☲☱☵☲☴☱☳☴☵☲☱☵☲☴☱☰☴☳☰☴☴☰☴☵☱☳☶☰☴☶☰☵☲☰☵☰☰☵☱')
    print(bArrToStr(arr))
